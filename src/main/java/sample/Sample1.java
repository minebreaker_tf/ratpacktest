package sample;

import ratpack.server.RatpackServer;

public final class Sample1 {

    public static void main( String[] args ) throws Exception {
        RatpackServer.start( server -> server
                .handlers( chain -> chain.get( ctx -> ctx.render( "hello, world" ) ) ) );
    }

}
