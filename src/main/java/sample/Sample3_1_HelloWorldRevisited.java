package sample;

import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.registry.Registry;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

public final class Sample3_1_HelloWorldRevisited {


    public static void main( String[] args ) throws Exception {

        ServerConfig config = ServerConfig.builder()
                                          .port( 5050 )
                                          .connectTimeoutMillis( 10000 )
                                          .threads( 8 )
                                          .development( true )
                                          .build();

        Registry registry = Registry.empty();

        Action<Chain> handlers = ( Chain chain ) -> {

            Handler handler = ( Context ctx ) -> {
                ctx.render( "hello, world" );
            };

            chain.get( handler );
        };

        RatpackServer.of( spec -> spec.serverConfig( config )
                                      .registry( registry )
                                      .handlers( handlers ) )
                     .start();
    }

}
