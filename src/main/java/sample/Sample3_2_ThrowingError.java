package sample;

import com.google.common.base.Throwables;
import ratpack.error.ServerErrorHandler;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.registry.Registry;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

public final class Sample3_2_ThrowingError {


    public static void main( String[] args ) throws Exception {

        ServerConfig config = ServerConfig.builder()
                                          .development( true )
                                          .build();

        Registry registry = Registry.single( new ServerErrorHandler() {
            @Override
            public void error( Context context, Throwable throwable ) throws Exception {
                context.render( Throwables.getStackTraceAsString( throwable ) );
            }
        } );

        Action<Chain> handlers = ( Chain chain ) -> {

            Handler handler = ( Context ctx ) -> {
                throw new Error( "Critical error!" );
            };

            chain.get( handler );
        };

        RatpackServer.of( spec -> spec.serverConfig( config )
                                      .registry( registry )
                                      .handlers( handlers ) )
                     .start();
    }

}
