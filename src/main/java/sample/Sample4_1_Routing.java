package sample;

import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.handling.Handler;
import ratpack.http.Response;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

public final class Sample4_1_Routing {

    public static void main( String[] args ) throws Exception {

        Handler handler = ctx -> ctx.getResponse().send( "Path: " + ctx.getRequest().getPath() );

        Action<Chain> handlers = chain -> {

//            chain.get( handler );
//            chain.get( "hoge", handler );
//            chain.when( ctx -> ctx.getRequest().getPath().startsWith( "hoge" ), innerChain -> {
//                innerChain.all( handler );
//            } );
//            chain.prefix( "hoge", innerChain -> {
//                innerChain.get( "piyo", handler );  // /hoge/piyo
//            } );
//            chain.path( "hoge", ctx -> ctx.parse( Form.class ).then( form -> {
//                ctx.render( "Received: " + form.entrySet() );
//            } ) );
//            chain.path( "hoge", ctx -> {
//                ctx.byMethod( m -> {
//                    m.get( iCtx -> iCtx.render( "get" ) );
//                    m.post( iCtx -> iCtx.render( "post" ) );
//                } );
//            } );
//            chain.path( "hoge", ctx -> ctx.byContent( accept -> {
//                accept.json( iCtx -> iCtx.render( "json" ) );
//                accept.xml( iCtx -> iCtx.render( "xml" ) );
//                accept.noMatch( iCtx -> iCtx.render( "no match" ) );
//            } ) );
//            chain.get( "hoge/:name", ctx -> ctx.render( "name: " + ctx.getPathTokens().get( "name" ) ) );
//            chain.get( "piyo/:id:\\d+", ctx -> ctx.render( "id: " + ctx.getPathTokens().asLong( "id" ) ) );

            chain.get( "hoge", ctx -> {

                String queryParam = ctx.getRequest().getQueryParams().get( "param" );
                Object hoge = ctx.header( "Accept" );
                Object piyo = ctx.getRequest().getHeaders().getNames();
                Object fuga = ctx.getRequest().getHeaders().get( "Accept" );

//                ctx.redirect( "" );
                Response r = ctx.getResponse();

                r.cookie( "hoge", "piyo" );
                r.getHeaders();
                r.contentType( "" );

                ctx.render( "hoge" );
            } );
        };

        RatpackServer.start( server -> server
                .serverConfig( ServerConfig.builder().development( true ) )
                .handlers( handlers ) );
    }

}
