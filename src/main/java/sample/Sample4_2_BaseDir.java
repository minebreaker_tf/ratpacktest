package sample;

import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.server.BaseDir;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

import java.nio.file.Path;

public final class Sample4_2_BaseDir {

    public static void main( String[] args ) throws Exception {

        Path baseDir = BaseDir.find( "public/.ratpack" );

        ServerConfig config = ServerConfig.builder()
                                          .baseDir( baseDir )
                                          .development( true ).build();

        Action<Chain> handlers = chain -> {
            chain.files();
            chain.get( "hoge", ctx -> ctx.render( ctx.file( "index.html" ) ) );
        };

        RatpackServer.start( server -> server
                .serverConfig( config )
                .handlers( handlers ) );
    }

}
