package sample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ratpack.func.Action;
import ratpack.handling.Chain;
import ratpack.jackson.Jackson;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

public final class Sample5_1_Json {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static final class User {
        private int id;
        private String name;
    }

    public static void main( String[] args ) throws Exception {

        Action<Chain> handlers = chain -> {
            chain.all( ctx -> {
                ctx.byMethod( m -> {
                    m.get( iCtx -> iCtx.render( Jackson.json( new User( 123, "John" ) ) ) );
//                    m.post( iCtx -> iCtx.parse( Jackson.fromJson( User.class ) ).then( user -> {
//                        iCtx.render( "Received user: " + user );
//                    } ) );
                    m.post( iCtx -> iCtx.parse( User.class ).then( user -> {
                        iCtx.render( "Received: " + user );
                    } ) );
                } );
            } );
        };

        RatpackServer.start( server -> server
                .serverConfig( ServerConfig.builder().development( true ).build() )
                .handlers( handlers ) );
    }
}
