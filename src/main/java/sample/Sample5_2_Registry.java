package sample;

import com.google.common.reflect.TypeToken;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ratpack.handling.Context;
import ratpack.handling.Handler;
import ratpack.http.TypedData;
import ratpack.parse.NoOptParserSupport;
import ratpack.render.RendererSupport;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;
import ratpack.util.Types;

public final class Sample5_2_Registry {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private static final class User {
        private int id;
        private String name;
    }

    public static final class UserRenderer extends RendererSupport<User> {
        @Override
        public void render( Context ctx, User user ) throws Exception {
            ctx.getResponse().contentType( "text/plain" );
            ctx.render( user.getId() + "," + user.getName() );
        }
    }

    public static final class UserParser extends NoOptParserSupport {
        @Override
        protected <T> T parse( Context context, TypedData requestBody, TypeToken<T> type ) throws Exception {
            if ( type.getRawType().equals( User.class ) ) {
                String[] csv = requestBody.getText().split( "," );
                User user = new User( Integer.parseInt( csv[0] ), csv[1] );
                return Types.cast( user );
            } else {
                return null;
            }
        }
    }

    public static final class CustomHandler implements Handler {
        @Override
        public void handle( Context ctx ) throws Exception {
            ctx.byMethod( m -> {
                m.get( iCtx -> {
                    iCtx.render( new User( 123, "John" ) );
                } );
                m.post( iCtx -> {
                    iCtx.parse( User.class ).then( user -> {
                        iCtx.render( "Received: " + user );
                    } );
                } );
            } );
        }
    }

    public static void main( String[] args ) throws Exception {
        RatpackServer.start( server -> server
                .serverConfig( ServerConfig.builder().development( true ) )
                .registryOf( spec -> {
                    spec.add( new UserRenderer() )
                        .add( new UserParser() )
                        .add( new CustomHandler() );
                } )
                .handler( CustomHandler.class ) );
    }

}
